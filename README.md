# easy-wms

Easy Warehouse Management System, build under license of Codeigniter 3 as main framework. The main purpose of these application that help user to get the right documentation and minimalizing every kind of manual work. 

# What is CodeIgniter

CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.

# Release Information

This repo contains in-development code for future releases. To download the latest stable release please visit the CodeIgniter Downloads page.

# Server Requirements

PHP version 5.6 or newer is recommended.

It should work on 5.4.8 as well, but we strongly advise you NOT to run such old versions of PHP, because of potential security and performance issues, as well as missing features.

# Installation Guidance

    1. Clone or download files. Add them to your Root folder if you are using XAMMP the location is "C:\xampp\htdocs\easy-wms".
    2. Open PhpMyAdmin from your local Application Server.
    3. Create new database and name it as "easy-wms"
    4. Import easy-wms.sql to your easy-wms database.
    5. Turn On Apache and MySQL on your Application Server Control Panel.
    6. Your system show be found on http://localhost/easy-wms/


# License

Under license of Anesthesia.

# Acknowledgement

First initiation.
